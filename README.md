# Note:

I've changed my family name

```diff
- Soiron
+ Schoenen
```

You can now tag me as `@Andyschoenen`

# How to collaborate with Andy

## Communication Style

- At work I prefer to communicate through written communications
- For social comminication I prefer to communicate verbally
- I can communicate well in large groups

## Working Style

- I benefit from work tasks being written or backed up with written communication
- I prefer to record zoom meetings so I can review them later
- I can handle multiple questions or instructions put to me at one time
- I tend to need time to process information before providing answers
- I prefer to have information or questions prior to meetings or discussions
- I work best when I can concentrate on a small number of tasks at one time

## Working style summary

I prefer a work environment that is structured, which leans heavily into asynchronous communication. If I get handed multiple tasks I will try to sort them by priority throught them in that order. I enjoy synchronous social interactions but prefere async communication for work.

## Feedback

- I prefer written feedback, followed up with a verbal conversation.
- You can share anonymous feedback with me using this link: https://forms.gle/DpbvnYhp9oWaJWfh9
